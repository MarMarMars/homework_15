﻿#include <iostream>
#include "Output.h"

int main()
{
    const int N = 21;

	std::cout << "Even numbers: \n";

	for (size_t i = 0; i <= N; i++)
	{
		if (i%2 == 0 ) {
			std::cout << i <<"\n";
		}

	}
    
	std::cout << "Even numbers ""For"": \n";
	outputFor(N, 0);

	std::cout << "Odd numbers ""For"": \n";
	outputFor(N, 1);

	std::cout << "Even numbers ""While"": \n";
	outputWhile(N, 0);

	std::cout << "Odd numbers ""While"": \n";
	outputWhile(N, 1);

}

