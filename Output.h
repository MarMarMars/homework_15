#pragma once

void outputFor(int N, int ost) {

	for (size_t i = 0; i <= N; i++)
	{
		if (i % 2 == ost) {
			std::cout << i << "\n";
		}

	}
}

void outputWhile(int N, int ost) {

	int i = ost;

	while (i <= N)
	{
		std::cout << i << "\n";
		i = i + 2;
	}
}
